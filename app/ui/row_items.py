# ----- imports -----
import os
import gi
# gi.require_version("Gtk", "4.0")
# from gi.repository import Gtk, Gio, Gdk
from gi.repository import Gtk


# ----- variables -----


# ----- functions ------
def get_file_loc():
    # get the current path on disk
    this_loc = os.path.dirname(os.path.abspath(__file__))
    return this_loc


# ----- classes -----

@Gtk.Template(filename=f'{get_file_loc()}/_row_items.ui')
class RowItem(Gtk.ListBoxRow):
    __gtype_name__ = "RowItem"

    ri_overall_hbox = Gtk.Template.Child()
    ri_whole_hbox = Gtk.Template.Child()
    ri_path_label = Gtk.Template.Child()
    ri_choices_hbox = Gtk.Template.Child()
    ri_d_button = Gtk.Template.Child()
    ri_rw_button = Gtk.Template.Child()
    ri_ro_button = Gtk.Template.Child()
    ri_delete_button = Gtk.Template.Child()

    def __init__(self, folder_path="/tmp", access_level="ro", **kwargs):
        Gtk.Box.__init__(self, **kwargs)
        self.folder_path = folder_path
        self.access_level = access_level
        self.ri_delete_button.set_halign(Gtk.Align(2))
        self.ri_path_label.set_label(self.get_display_name())
        self.set_buttons_group()
        self.set_access_level_toggle()

    def set_buttons_group(self):
        # group the rw/ro buttons to the d (disabled) button
        self.ri_rw_button.set_group(self.ri_d_button)
        self.ri_ro_button.set_group(self.ri_d_button)

    def set_access_level_toggle(self):
        # check the access_level, set the corresponding radio button as toggled
        if self.access_level == "ro":
            self.ri_ro_button.set_active(True)
        elif self.access_level == "rw":
            self.ri_rw_button.set_active(True)
        else:
            self.ri_d_button.set_active(True)

    def get_display_name(self):
        # get the last part of the path, to use as
        # the display name in the gui
        path_split = self.folder_path.split('/')
        return path_split.pop((len(path_split) - 1))

    def get_del_button_reference(self):
        # get the delete button reference, to connect the
        # remove item function
        return self.ri_delete_button

    def get_access_level(self):
        # return which of the toggles is active
        # so it cane be save in user_prefs
        if self.ri_ro_button.get_active():
            return "ro"
        elif self.ri_rw_button.get_active():
            return "rw"
        else:
            return "disabled"

    def get_references_for_toggle_buttons(self):
        # pass a list of the buttons, so we can connect the toggled signal
        # to the save button color change
        return [self.ri_d_button, self.ri_ro_button, self.ri_rw_button]

