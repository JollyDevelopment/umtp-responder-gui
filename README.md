# umtp-responder-gui
GUI for uMtp-Responder for PinePhone

A GTK app to control the folders exposed from a pinephone to a PC when connected via USB.

This uses the [uMTP-Responder by Viveris](https://github.com/viveris/uMTP-Responder) under the hood. Inspired by the way Mobian used to do it, as discussed [here](https://forum.pine64.org/showthread.php?tid=12633) and [here](https://gitlab.com/mobian1/issues/-/issues/232).

### Features

* Enable / Disable the service
* Start / Stop / Restart the service
* Add up to 16 folders
* Control Access Level per folder
  * Read Only
  * Read / Write
  * Disabled

### Caveat

The files written to disk are owned by root, so you'll have to update their permissions manually to your user. There are a couple of issues in Viveris' repo ([here](https://github.com/viveris/uMTP-Responder/issues/48) and [here](https://github.com/viveris/uMTP-Responder/issues/35)) where this has been discussed.  NOTE: Viveris has implemented a uid/guid function, so I will be updating the generated config to add support for that in the next release.


### Screenshots

![uMTP-Responder-Control](imgs/screenshot_01.png?raw=true)
![uMTP-Responder-Control](imgs/screenshot_02.png?raw=true)
![uMTP-Responder-Control](imgs/screenshot_03.png?raw=true)


### Installation

I am using ArchLinuxArm on my PinePhone, so I have a PKGBUILD [here](releases/PKGBUILD).
The app /should/ work on other distros, but you'd have to install it yourself.

### Requirements

* python 3+
* Gtk4
* libadwaita
* build umtp-responder. Viveris' repo has good instructions on that.

